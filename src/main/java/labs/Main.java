package labs;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class Main {
    private static final Logger logger = LogManager.getLogger("HelloWorld");
    public static void main(String[] args) {
        // ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        logger.info("Hello, World!");

    }
}